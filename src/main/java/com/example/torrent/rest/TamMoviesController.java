package com.example.torrent.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.example.torrent.model.*;
import com.example.torrent.repo.*;

import javax.validation.Valid;

import java.util.List;

@RestController
@CrossOrigin("*")

@RequestMapping("/api")

public class TamMoviesController {

	@Autowired
	TamTorrentRepository tamTorrentRepo;

	@GetMapping("/getTamMovies")
	public List<TamMovies> getTamMovies() {

		return tamTorrentRepo.findAll();
	}

	@PostMapping("/createTamMovies")
	public @Valid TamMovies createEngMovies(@Valid @RequestBody TamMovies TamMovies) {
		return tamTorrentRepo.save(TamMovies);
	}

	@GetMapping("/TamMovie/{id}")
	public TamMovies getById(@PathVariable(required = true) long id) {
		return tamTorrentRepo.getById(id);
	}

	@PutMapping("/updateTamMovies/{id}")
	public TamMovies updateEngMovies(@PathVariable(value = "id") Long Id, @Valid @RequestBody TamMovies Movies) {
		TamMovies updatedNote = tamTorrentRepo.save(Movies);
		return updatedNote;
	}

	@DeleteMapping("/tamDelete/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long Id) {
		tamTorrentRepo.deleteById(Id);
		return ResponseEntity.ok().build();
	}

}