package com.example.torrent.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.torrent.model.OtherLangMovies;
import com.example.torrent.repo.OtherLangMoviesRepository;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class OtherLangMoviesController {

	@Autowired
	OtherLangMoviesRepository otherLangMoviesRepo;

	@GetMapping(value = "/getOtherLangMovies")
	public List<OtherLangMovies> getEngMovies() {
		return otherLangMoviesRepo.findAll();
	}

	@PostMapping(value = "/createOtherLangMovies")
	public @Valid OtherLangMovies createEngMovies(@Valid @RequestBody OtherLangMovies otherLangMovies) {
		return otherLangMoviesRepo.save(otherLangMovies);
	}

	@GetMapping("/OtherLangMovies/{id}")
	public OtherLangMovies getById(@PathVariable(required = true) long id) {
		return otherLangMoviesRepo.getById(id);
	}

	@PutMapping("/updateOtherLangMovies/{id}")
	public OtherLangMovies updateEngMovies(@PathVariable(value = "id") Long Id, @Valid @RequestBody OtherLangMovies otherLangMovies) {
		OtherLangMovies updatedNote = otherLangMoviesRepo.save(otherLangMovies);
		return updatedNote;
	}

	@DeleteMapping("/otherLangMoviesDelete/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long Id) {
		otherLangMoviesRepo.deleteById(Id);
		return ResponseEntity.ok().build();
	}
}
