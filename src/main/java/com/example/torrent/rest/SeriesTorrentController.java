package com.example.torrent.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.torrent.model.SeriesTorrent;
import com.example.torrent.repo.SeriesTorrentRepository;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class SeriesTorrentController {
	
	@Autowired
	SeriesTorrentRepository seriesTorrentRepo;
	
	@GetMapping(value = "/getEngSeries")
	public List<SeriesTorrent> getEngSeries() {
		return seriesTorrentRepo.findAll();
	}

	@PostMapping(value = "/createEngSeries")
	public @Valid SeriesTorrent createEngMovies(@Valid @RequestBody SeriesTorrent series) {
		return seriesTorrentRepo.save(series);
	}

	@GetMapping("/EngSeries/{id}")
	public SeriesTorrent getById(@PathVariable(required = true) long id) {
		return seriesTorrentRepo.getById(id);
	}
	
	@GetMapping("/Series/{seriesTitle}")
	public List<SeriesTorrent> getByseriesTitle(@PathVariable(required = true) String seriesTitle) {
		return seriesTorrentRepo.findBySeriesTitle(seriesTitle);
	}

	@PutMapping("/updateEngSeries/{id}")
	public SeriesTorrent updateEngSeries(@PathVariable(value = "id") Long Id, @Valid @RequestBody SeriesTorrent series) {
		SeriesTorrent updatedBody = seriesTorrentRepo.save(series);
		return updatedBody;
	}

	@DeleteMapping("/engSeriesDelete/{id}")
	public ResponseEntity<?> deleteSeries(@PathVariable(value = "id") Long Id) {
		seriesTorrentRepo.deleteById(Id);
		return ResponseEntity.ok().build();
	}

}
