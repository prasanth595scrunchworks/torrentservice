package com.example.torrent.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "m_eng_series", schema = "torrentRockers")
public class SeriesTorrent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "seriesName")
	private String seriesName;

	@Column(name = "seriesImg")
	private String seriesImg;
	
	@Column(name = "seriesUrl")
	private String seriesUrl;
	
	@Column(name = "seriesTitle")
	private String seriesTitle;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public String getSeriesImg() {
		return seriesImg;
	}

	public void setSeriesImg(String seriesImg) {
		this.seriesImg = seriesImg;
	}

	public String getSeriesUrl() {
		return seriesUrl;
	}

	public void setSeriesUrl(String seriesUrl) {
		this.seriesUrl = seriesUrl;
	}

	public String getSeriesTitle() {
		return seriesTitle;
	}

	public void setSeriesTitle(String seriesTitle) {
		this.seriesTitle = seriesTitle;
	}
	

}
