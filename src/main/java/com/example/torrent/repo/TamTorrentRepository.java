package com.example.torrent.repo;


import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.torrent.*;
import com.example.torrent.model.EngMovies;
import com.example.torrent.model.TamMovies;


@Repository
public interface TamTorrentRepository extends JpaRepository<TamMovies, Long> {

	TamMovies getById(long id);


	
}
