package com.example.torrent.repo;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.torrent.model.SeriesListTorrent;
import com.example.torrent.model.SeriesTorrent;

@Repository
public interface SeriesListRepository extends JpaRepository<SeriesListTorrent, Long> {

	SeriesListTorrent save(@Valid SeriesListTorrent series);


}
