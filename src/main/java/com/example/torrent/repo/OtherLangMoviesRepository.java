package com.example.torrent.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.torrent.model.OtherLangMovies;

@Repository
public interface OtherLangMoviesRepository extends JpaRepository<OtherLangMovies, Long> {

	void deleteById(Optional<OtherLangMovies> note);


	OtherLangMovies getById(long id);

}
