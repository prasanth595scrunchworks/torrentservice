package com.example.torrent.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.torrent.model.SeriesTorrent;

@Repository
public interface  SeriesTorrentRepository extends JpaRepository<SeriesTorrent, Long> {

	SeriesTorrent getById(long id);
	
	List<SeriesTorrent> 	findBySeriesTitle(String seriesTitle);

}
