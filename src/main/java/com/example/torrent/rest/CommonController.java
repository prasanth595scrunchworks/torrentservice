package com.example.torrent.rest;

import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class CommonController {

	@GetMapping(value = "/ip")
	public Map<String, String> sayHello(HttpServletRequest request, HttpServletResponse response) {
		HashMap<String, String> map = new HashMap<>();
		String clintHost = request.getRemoteHost();
		map.put("IpAddress", clintHost);
		return map;
	}
}