package com.example.torrent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorrentRockersSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TorrentRockersSpringBootApplication.class, args);
	}

}
