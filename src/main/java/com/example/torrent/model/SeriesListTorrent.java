package com.example.torrent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_eng_series_List", schema = "torrentRockers")

public class SeriesListTorrent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "seriesList")
	private String seriesTitle;
	
	@Column(name = "seriesListImg")
	private String seriesImg;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSeriesTitle() {
		return seriesTitle;
	}

	public void setSeriesTitle(String seriesTitle) {
		this.seriesTitle = seriesTitle;
	}

	public String getSeriesImg() {
		return seriesImg;
	}

	public void setSeriesImg(String seriesImg) {
		this.seriesImg = seriesImg;
	}

	
	
	

}
