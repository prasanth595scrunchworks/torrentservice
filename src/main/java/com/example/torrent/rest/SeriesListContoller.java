package com.example.torrent.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.torrent.model.SeriesListTorrent;
import com.example.torrent.model.SeriesTorrent;
import com.example.torrent.repo.SeriesListRepository;
import com.example.torrent.repo.SeriesTorrentRepository;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class SeriesListContoller {

	@Autowired
	SeriesListRepository seriesListRepository;

	@GetMapping(value = "/getSeriesList")
	public List<SeriesListTorrent> getEngMovies() {
		return seriesListRepository.findAll();
	}
	
	@PostMapping(value = "/createSeriesTitle")
	public @Valid SeriesListTorrent createEngMovies(@Valid @RequestBody SeriesListTorrent series) {
		return seriesListRepository.save(series);
	}
	
	@PutMapping("/updateSeriesList/{id}")
	public SeriesListTorrent updateSeriesList(@PathVariable(value = "id") Long Id, @Valid @RequestBody SeriesListTorrent series) {
		SeriesListTorrent updatedBody = seriesListRepository.save(series);
		return updatedBody;
	}

	@DeleteMapping("/engSeriesListDelete/{id}")
	public ResponseEntity<?> deleteSeries(@PathVariable(value = "id") Long Id) {
		seriesListRepository.deleteById(Id);
		return ResponseEntity.ok().build();
	}

}
