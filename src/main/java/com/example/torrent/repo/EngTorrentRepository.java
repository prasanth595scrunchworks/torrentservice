package com.example.torrent.repo;



import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.torrent.model.*;


@Repository
public interface EngTorrentRepository extends JpaRepository<EngMovies, Long> {

	void deleteById(Optional<EngMovies> note);


	EngMovies getById(long id);


}

