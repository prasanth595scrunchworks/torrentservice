package com.example.torrent.rest;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.example.torrent.model.*;
import com.example.torrent.repo.*;

import javax.validation.Valid;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class EngMoviesController {

	@Autowired
	EngTorrentRepository engTorrentRepo;

	@GetMapping(value = "/getEngMovies")
	public List<EngMovies> getEngMovies() {
		return engTorrentRepo.findAll();
	}

	@PostMapping(value = "/createEngMovies")
	public @Valid EngMovies createEngMovies(@Valid @RequestBody EngMovies engMovies) {
		return engTorrentRepo.save(engMovies);
	}

	@GetMapping("/EngMovie/{id}")
	public EngMovies getById(@PathVariable(required = true) long id) {
		return engTorrentRepo.getById(id);
	}

	@PutMapping("/updateEngMovies/{id}")
	public EngMovies updateEngMovies(@PathVariable(value = "id") Long Id, @Valid @RequestBody EngMovies Movies) {
		EngMovies updatedNote = engTorrentRepo.save(Movies);
		return updatedNote;
	}

	@DeleteMapping("/engDelete/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long Id) {
		engTorrentRepo.deleteById(Id);
		return ResponseEntity.ok().build();
	}

}